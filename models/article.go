package models

import "time"

type Article struct {
	ID        string    `db:"id"`
	AuthorID  string    `db:"author_id"`
	Title     string    `db:"title"`
	Body      string    `db:"body"`
	CreatedAt time.Time `db:"created_at"`
}

type ArticleResponse struct {
	ID        string    `json:"id"`
	Title     string    `json:"title"`
	Body      string    `json:"body"`
	CreatedAt time.Time `json:"createdAt"`
	Author    *Author    `json:"author"`
}

type CreateArticlePayload struct {
	Author   string `json:"author" validate:"required"`
	AuthorID string `json:"-"`
	Title    string `json:"title" validate:"required"`
	Body     string `json:"body" validate:"required"`
}

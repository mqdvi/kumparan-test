package config

import (
	"log"

	"github.com/kelseyhightower/envconfig"
)

type configuration struct {
	Database databaseConfig
}

type databaseConfig struct {
	Hostname string `envconfig:"DATABASE_HOSTNAME" default:"localhost"`
	Username string `envconfig:"DATABASE_USERNAME" default:"root"`
	Password string `envconfig:"DATABASE_PASSWORD" default:""`
	Database string `envconfig:"DATABASE_NAME" default:"db_test_kumparan"`
	Port     int    `envconfig:"DATABASE_PORT" default:"3306"`

	Timeout           string `envconfig:"DATABASE_TIMEOUT" default:"30s"`
	MaxOpenConnection int    `envconfig:"DATABASE_MAX_OPEN_CONNECTION" default:"10"`
	MaxIdleConnection int    `envconfig:"DATABASE_MAX_IDLE_CONNECTION" default:"10"`
}

var Config configuration

func init() {
	log.Println("Load configuration...")

	if err := envconfig.Process("", &Config); err != nil {
		log.Fatalf("Error when loading configuration: %v", err)
	}
}

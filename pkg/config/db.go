package config

import (
	"fmt"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

func GetDBClient() (db *sqlx.DB) {
	connectionString := fmt.Sprintf(
		"%s:%s@tcp(%s:%v)/%s?parseTime=true",
		Config.Database.Username,
		Config.Database.Password,
		Config.Database.Hostname,
		Config.Database.Port,
		Config.Database.Database,
	)

	db, err := sqlx.Open("mysql", connectionString)
	if err != nil {
		log.Fatalf("Error when creating DB connection: %v", err)
	}

	parseTimeout, _ := time.ParseDuration(Config.Database.Timeout)

	db.SetConnMaxLifetime(parseTimeout)
	db.SetMaxOpenConns(Config.Database.MaxOpenConnection)
	db.SetMaxIdleConns(Config.Database.MaxIdleConnection)

	err = db.Ping()
	if err != nil {
		log.Fatalf("Error during ping database: %v", err)
	}

	return db
}

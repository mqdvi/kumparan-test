package utils

import (
	"math"
	"net/url"
	"reflect"

	"github.com/gin-gonic/gin"
)

const defaultMaxLimit = 50

type Meta struct {
	Count      uint64      `json:"totalItems"`
	TotalPages uint64      `json:"totalPages"`
	Page       uint64      `json:"currentPage"`
	Limit      uint64      `json:"itemsPerPage"`
	PrevLink   interface{} `json:"previousLink"`
	NextLink   interface{} `json:"nextLink"`
	MaxLimit   uint64      `json:"-"`
	Offset     uint64      `json:"-"`
	Sort       string      `json:"-"`
	Params     url.Values  `json:"-"`
	BaseURL    string      `json:"-"`
	URL        *url.URL    `json:"-"`
}

// NewMeta meta for pagination with default baseURL and Limit
func NewMeta(baseURL string, limit uint64) *Meta {
	return &Meta{
		BaseURL:  baseURL,
		Limit:    limit,
		MaxLimit: defaultMaxLimit,
	}
}

func (m *Meta) WithMaxLimit(maxLimit uint64) *Meta {
	if maxLimit < m.Limit {
		m.MaxLimit = m.Limit
	} else {
		m.MaxLimit = maxLimit
	}

	return m
}

// FromContext retrieve pagination meta from echo context
func (m *Meta) FromContext(c *gin.Context) *Meta {
	m.Params = c.Request.URL.Query()
	m.URL = c.Request.URL
	m.Sort = c.Query("sort")

	page := StringToUint64(c.Query("page"))
	if page == 0 {
		m.Page = 1
	} else {
		m.Page = page
	}

	if m.MaxLimit == 0 {
		m.MaxLimit = defaultMaxLimit
	}

	limit := StringToUint64(c.Query("itemsPerPage"))
	if limit != 0 {
		if limit > m.MaxLimit {
			limit = m.MaxLimit
		}

		m.Limit = limit
	}

	if m.Page == 0 {
		m.Offset = 0
	} else {
		m.Offset = (m.Page - 1) * m.Limit
	}

	if m.Page > 1 {
		newPage := Uint64ToString(m.Page - 1)
		m.Params.Set("page", newPage)
		m.Params.Set("itemsPerPage", Uint64ToString(m.Limit))

		m.PrevLink = m.BaseURL + m.URL.Path + "?" + m.Params.Encode()
	}

	return m
}

// SetNextLink ...
func (m *Meta) SetNextLink() {
	if m.Page >= m.TotalPages {
		return
	}
	newPage := Uint64ToString(m.Page + 1)
	m.Params.Set("page", newPage)
	m.Params.Set("itemsPerPage", Uint64ToString(m.Limit))

	m.NextLink = m.BaseURL + m.URL.Path + "?" + m.Params.Encode()
}

// SetTotalPages ...
func (m *Meta) SetTotalPages() {
	m.TotalPages = uint64(math.Ceil(float64(m.Count) / float64(m.Limit)))
}

type Data struct {
	*Meta `json:",omitempty"`
	Items interface{} `json:"items,omitempty"`
}

func (d Data) WithMeta(m *Meta) Data {
	d.Meta = m
	d.Meta.SetTotalPages()
	d.Meta.SetNextLink()
	return d
}

func (d Data) WithItems(i interface{}) Data {
	val := reflect.ValueOf(i)
	switch val.Kind() {
	// if slice or array
	case reflect.Slice, reflect.Array:
		if val.Len() == 0 {
			// Empty slice/array -> []
			d.Items = []bool{}
		} else {
			d.Items = i
		}
	case reflect.Struct:
		// if single struct should convert into -> [struct]
		d.Items = []interface{}{i}
	default:
		// nil
		d.Items = []bool{}
	}

	return d
}

type PaginationResponse struct {
	Data Data `json:"data"`
}

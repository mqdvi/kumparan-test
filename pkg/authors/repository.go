package authors

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"gitlab.com/mqdvi/kumparan-test/models"
)

type authorRepository struct {
	db *sqlx.DB
}

func NewRepository(db *sqlx.DB) *authorRepository {
	return &authorRepository{db: db}
}

func (repo *authorRepository) GetAuthor(ctx context.Context, column, value string) (*models.Author, error) {
	query := fmt.Sprintf(`
		SELECT
			id,
			name
		FROM
			authors
		WHERE
			%s = ?
	`, column)

	var author models.Author
	if err := repo.db.GetContext(ctx, &author, query, value); err != nil {
		return nil, err
	}

	return &author, nil
}

func (repo *authorRepository) Create(ctx context.Context, name string) (*models.Author, error) {
	query := `
		INSERT INTO 
			authors (id, name) 
		VALUES
			(?, ?)		
	`

	if _, err := repo.db.ExecContext(ctx, query, uuid.NewString(), name); err != nil {
		return nil, err
	}

	return repo.GetAuthor(ctx, "name", name)
}

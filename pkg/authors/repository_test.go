package authors_test

import (
	"context"
	"database/sql"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"

	"gitlab.com/mqdvi/kumparan-test/models"
	"gitlab.com/mqdvi/kumparan-test/pkg/authors"
)

func Test_authorRepository_GetAuthor(t *testing.T) {
	type (
		args struct {
			ctx    context.Context
			column string
			value  string
		}
		mockGet struct {
			result *models.Author
			err    error
		}
	)

	tests := []struct {
		name          string
		args          args
		mockGet       mockGet
		expectedQuery string
		want          *models.Author
		wantErr       error
	}{
		{
			name: "success get by id",
			args: args{
				ctx:    context.Background(),
				column: "id",
				value:  "author-id",
			},
			mockGet: mockGet{
				result: &models.Author{
					ID:   "author-id",
					Name: "Nayeon",
				},
			},
			expectedQuery: "SELECT id, name FROM authors WHERE id = ?",
			want: &models.Author{
				ID:   "author-id",
				Name: "Nayeon",
			},
			wantErr: nil,
		},
		{
			name: "success get by name",
			args: args{
				ctx:    context.Background(),
				column: "name",
				value:  "Nayeon",
			},
			mockGet: mockGet{
				result: &models.Author{
					ID:   "author-id",
					Name: "Nayeon",
				},
			},
			expectedQuery: "SELECT id, name FROM authors WHERE name = ?",
			want: &models.Author{
				ID:   "author-id",
				Name: "Nayeon",
			},
			wantErr: nil,
		},
		{
			name: "error",
			args: args{
				ctx:    context.Background(),
				column: "name",
				value:  "Nayeon",
			},
			mockGet:       mockGet{err: sql.ErrNoRows},
			expectedQuery: "SELECT id, name FROM authors WHERE name = ?",
			want:          nil,
			wantErr:       sql.ErrNoRows,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()
			sqlxDB := sqlx.NewDb(db, "sqlmock")

			repo := authors.NewRepository(sqlxDB)

			mockExpectQuery := mock.ExpectQuery(tc.expectedQuery)
			if tc.mockGet.err != nil {
				mockExpectQuery.WillReturnError(tc.mockGet.err)
			} else {
				row := sqlmock.NewRows([]string{
					"id",
					"name",
				})
				author := (*tc.mockGet.result)
				row.AddRow(author.ID, author.Name)
				mockExpectQuery.WillReturnRows(row)
			}

			got, err := repo.GetAuthor(tc.args.ctx, tc.args.column, tc.args.value)
			assert.Equal(t, tc.want, got, tc.name)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}

func Test_authorRepository_Create(t *testing.T) {
	type (
		args struct {
			ctx  context.Context
			name string
		}
		mockExec struct {
			err error
		}
		mockGetAuthor struct {
			result *models.Author
			err    error
		}
	)

	tests := []struct {
		name          string
		args          args
		mockExec      mockExec
		mockGetAuthor *mockGetAuthor
		want          *models.Author
		wantErr       error
	}{
		{
			name: "success",
			args: args{
				ctx:  context.Background(),
				name: "Nayeon",
			},
			mockExec: mockExec{err: nil},
			mockGetAuthor: &mockGetAuthor{
				result: &models.Author{
					ID:   "author-id",
					Name: "Nayeon",
				},
			},
			want: &models.Author{
				ID:   "author-id",
				Name: "Nayeon",
			},
			wantErr: nil,
		},
		{
			name: "error",
			args: args{
				ctx:  context.Background(),
				name: "Nayeon",
			},
			mockExec: mockExec{err: sql.ErrConnDone},
			wantErr:  sql.ErrConnDone,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()
			sqlxDB := sqlx.NewDb(db, "sqlmock")

			repo := authors.NewRepository(sqlxDB)

			mockExpectExec := mock.ExpectExec(`INSERT INTO authors (id, name) VALUES (?, ?)`).WithArgs(
				sqlmock.AnyArg(),
				tc.args.name,
			)

			if tc.mockExec.err != nil {
				mockExpectExec.WillReturnError(tc.mockExec.err)
			} else {
				mockExpectExec.WillReturnResult(sqlmock.NewResult(1, 1))
				getAuthor(mock, tc.args.name, tc.mockGetAuthor.result, tc.mockGetAuthor.err)
			}

			got, err := repo.Create(tc.args.ctx, tc.args.name)
			assert.Equal(t, tc.want, got)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}

func getAuthor(mock sqlmock.Sqlmock, name string, author *models.Author, err error) {
	mockExpectQuery := mock.ExpectQuery(`SELECT id, name FROM authors WHERE name = ?`).
		WithArgs(name)
	if err != nil {
		mockExpectQuery.WillReturnError(err)
	} else {
		row := sqlmock.NewRows([]string{
			"id",
			"name",
		})
		row.AddRow(author.ID, author.Name)
		mockExpectQuery.WillReturnRows(row)
	}
}

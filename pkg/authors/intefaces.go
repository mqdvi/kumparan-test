package authors

import (
	"context"

	"gitlab.com/mqdvi/kumparan-test/models"
)

type AuthorRepositoryInterface interface {
	GetAuthor(ctx context.Context, column, value string) (*models.Author, error)
	Create(ctx context.Context, name string) (*models.Author, error)
}

package articles

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"

	"gitlab.com/mqdvi/kumparan-test/models"
	"gitlab.com/mqdvi/kumparan-test/pkg/utils"
)

type articleController struct {
	articleSvc ArticleServiceInterface
}

func NewController(articleSvc ArticleServiceInterface) *articleController {
	return &articleController{articleSvc: articleSvc}
}

func (ctrl *articleController) GetArticles(c *gin.Context) {
	var limit uint64
	limitStr := c.Query("itemsPerPage")

	limitInt := utils.StringToUint64(limitStr)
	if limitInt > 0 {
		limit = limitInt
	} else {
		limit = uint64(100)
	}

	query := c.Query("query")
	author := c.Query("author")

	paging := utils.NewMeta("http://localhost:4200", limit)
	paging.FromContext(c)

	result, err := ctrl.articleSvc.GetArticles(c, &query, &author, paging)
	if err != nil {
		errorResponse := utils.NewErrorResponse("articles-500", err.Error())

		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}

	c.JSON(http.StatusOK, result)
}

func (ctrl *articleController) Create(c *gin.Context) {
	body := new(models.CreateArticlePayload)
	if err := c.ShouldBindJSON(&body); err != nil {
		errorResponse := utils.NewErrorResponse("articles-500", err.Error())

		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}
	if err := validator.New().Struct(body); err != nil {
		errorResponse := utils.NewErrorResponse("articles-500", err.Error())

		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}

	err := ctrl.articleSvc.Create(c, body)
	if err != nil {
		errorResponse := utils.NewErrorResponse("articles-500", err.Error())

		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}

	c.JSON(http.StatusCreated, utils.NewJsonResponse(map[string]interface{}{
		"message": "Article successfully created",
	}))
}

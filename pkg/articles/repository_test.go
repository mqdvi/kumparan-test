package articles_test

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"

	"gitlab.com/mqdvi/kumparan-test/models"
	"gitlab.com/mqdvi/kumparan-test/pkg/articles"
	"gitlab.com/mqdvi/kumparan-test/pkg/utils"
)

func Test_articleRepository_GetArticles(t *testing.T) {
	var (
		search    = "test"
		author    = "Test"
		createdAt = time.Now()
	)

	type (
		args struct {
			ctx    context.Context
			search *string
			author *string
			meta   *utils.Meta
		}
		mockGet struct {
			result []models.Article
			err    error
		}
	)

	tests := []struct {
		name          string
		args          args
		mockGet       mockGet
		expectedQuery string
		want          []models.Article
		wantErr       error
	}{
		{
			name: "success",
			args: args{
				ctx: context.Background(),
				meta: &utils.Meta{
					Limit:  1,
					Offset: 0,
				},
			},
			mockGet: mockGet{
				result: []models.Article{
					{
						ID:        "article-id",
						AuthorID:  "author-id",
						Title:     "Test",
						Body:      "Test",
						CreatedAt: createdAt,
					},
				},
			},
			expectedQuery: "SELECT articles.id, articles.author_id, articles.title, articles.body, articles.created_at FROM articles ORDER BY articles.created_at DESC LIMIT ? OFFSET ?",
			want: []models.Article{
				{
					ID:        "article-id",
					AuthorID:  "author-id",
					Title:     "Test",
					Body:      "Test",
					CreatedAt: createdAt,
				},
			},
			wantErr: nil,
		},
		{
			name: "success with query",
			args: args{
				ctx:    context.Background(),
				search: &search,
				meta: &utils.Meta{
					Limit:  1,
					Offset: 0,
				},
			},
			mockGet: mockGet{
				result: []models.Article{
					{
						ID:        "article-id",
						AuthorID:  "author-id",
						Title:     "Test",
						Body:      "Test",
						CreatedAt: createdAt,
					},
				},
			},
			expectedQuery: "SELECT articles.id, articles.author_id, articles.title, articles.body, articles.created_at FROM articles WHERE (body LIKE ? OR title LIKE ?) ORDER BY articles.created_at DESC LIMIT ? OFFSET ?",
			want: []models.Article{
				{
					ID:        "article-id",
					AuthorID:  "author-id",
					Title:     "Test",
					Body:      "Test",
					CreatedAt: createdAt,
				},
			},
			wantErr: nil,
		},
		{
			name: "success with filter",
			args: args{
				ctx:    context.Background(),
				author: &author,
				meta: &utils.Meta{
					Limit:  1,
					Offset: 0,
				},
			},
			mockGet: mockGet{
				result: []models.Article{
					{
						ID:        "article-id",
						AuthorID:  "author-id",
						Title:     "Test",
						Body:      "Test",
						CreatedAt: createdAt,
					},
				},
			},
			expectedQuery: "SELECT articles.id, articles.author_id, articles.title, articles.body, articles.created_at FROM articles WHERE EXISTS( SELECT * FROM authors WHERE articles.author_id = authors.id AND authors.name = ? ) ORDER BY articles.created_at DESC LIMIT ? OFFSET ?",
			want: []models.Article{
				{
					ID:        "article-id",
					AuthorID:  "author-id",
					Title:     "Test",
					Body:      "Test",
					CreatedAt: createdAt,
				},
			},
			wantErr: nil,
		},
		{
			name: "success with filter and search",
			args: args{
				ctx:    context.Background(),
				author: &author,
				search: &search,
				meta: &utils.Meta{
					Limit:  1,
					Offset: 0,
				},
			},
			mockGet: mockGet{
				result: []models.Article{
					{
						ID:        "article-id",
						AuthorID:  "author-id",
						Title:     "Test",
						Body:      "Test",
						CreatedAt: createdAt,
					},
				},
			},
			expectedQuery: "SELECT articles.id, articles.author_id, articles.title, articles.body, articles.created_at FROM articles WHERE (body LIKE ? OR title LIKE ?) AND EXISTS( SELECT * FROM authors WHERE articles.author_id = authors.id AND authors.name = ? ) ORDER BY articles.created_at DESC LIMIT ? OFFSET ?",
			want: []models.Article{
				{
					ID:        "article-id",
					AuthorID:  "author-id",
					Title:     "Test",
					Body:      "Test",
					CreatedAt: createdAt,
				},
			},
			wantErr: nil,
		},
		{
			name: "error",
			args: args{
				ctx: context.Background(),
				meta: &utils.Meta{
					Limit:  1,
					Offset: 0,
				},
			},
			mockGet: mockGet{
				err: sql.ErrNoRows,
			},
			expectedQuery: "SELECT articles.id, articles.author_id, articles.title, articles.body, articles.created_at FROM articles ORDER BY articles.created_at DESC LIMIT ? OFFSET ?",
			wantErr:       sql.ErrNoRows,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()
			sqlxDB := sqlx.NewDb(db, "sqlmock")

			repo := articles.NewRepository(sqlxDB)

			mockExpectQuery := mock.ExpectQuery(tc.expectedQuery)
			if tc.mockGet.err != nil {
				mockExpectQuery.WillReturnError(tc.mockGet.err)
			} else {
				row := sqlmock.NewRows([]string{
					"id",
					"author_id",
					"body",
					"title",
					"created_at",
				})
				article := (tc.mockGet.result)[0]
				row.AddRow(article.ID, article.AuthorID, article.Body, article.Title, article.CreatedAt)
				mockExpectQuery.WillReturnRows(row)
			}

			got, err := repo.GetArticles(tc.args.ctx, tc.args.search, tc.args.author, tc.args.meta)
			assert.Equal(t, tc.want, got, tc.name)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}

func Test_articleRepository_GetArticlesCount(t *testing.T) {
	var (
		search = "test"
		author = "Test"
	)

	type (
		args struct {
			ctx    context.Context
			search *string
			author *string
			meta   *utils.Meta
		}
		mockGet struct {
			result int64
			err    error
		}
	)

	tests := []struct {
		name          string
		args          args
		mockGet       mockGet
		expectedQuery string
		want          int64
		wantErr       error
	}{
		{
			name: "success",
			args: args{
				ctx: context.Background(),
				meta: &utils.Meta{
					Limit:  1,
					Offset: 0,
				},
			},
			mockGet: mockGet{
				result: 1,
			},
			expectedQuery: "SELECT COUNT(articles.id) FROM articles ORDER BY articles.created_at DESC LIMIT ? OFFSET ?",
			want:          1,
			wantErr:       nil,
		},
		{
			name: "success with query",
			args: args{
				ctx:    context.Background(),
				search: &search,
				meta: &utils.Meta{
					Limit:  1,
					Offset: 0,
				},
			},
			mockGet: mockGet{
				result: 1,
			},
			expectedQuery: "SELECT COUNT(articles.id) FROM articles WHERE (body LIKE ? OR title LIKE ?) ORDER BY articles.created_at DESC LIMIT ? OFFSET ?",
			want:          1,
			wantErr:       nil,
		},
		{
			name: "success with filter",
			args: args{
				ctx:    context.Background(),
				author: &author,
				meta: &utils.Meta{
					Limit:  1,
					Offset: 0,
				},
			},
			mockGet: mockGet{
				result: 1,
			},
			expectedQuery: "SELECT COUNT(articles.id) FROM articles WHERE EXISTS( SELECT * FROM authors WHERE articles.author_id = authors.id AND authors.name = ? ) ORDER BY articles.created_at DESC LIMIT ? OFFSET ?",
			want:          1,
			wantErr:       nil,
		},
		{
			name: "success with filter and search",
			args: args{
				ctx:    context.Background(),
				author: &author,
				search: &search,
				meta: &utils.Meta{
					Limit:  1,
					Offset: 0,
				},
			},
			mockGet: mockGet{
				result: 1,
			},
			expectedQuery: "SELECT COUNT(articles.id) FROM articles WHERE (body LIKE ? OR title LIKE ?) AND EXISTS( SELECT * FROM authors WHERE articles.author_id = authors.id AND authors.name = ? ) ORDER BY articles.created_at DESC LIMIT ? OFFSET ?",
			want:          1,
			wantErr:       nil,
		},
		{
			name: "error",
			args: args{
				ctx: context.Background(),
				meta: &utils.Meta{
					Limit:  1,
					Offset: 0,
				},
			},
			mockGet: mockGet{
				err: sql.ErrNoRows,
			},
			expectedQuery: "SELECT COUNT(articles.id) FROM articles ORDER BY articles.created_at DESC LIMIT ? OFFSET ?",
			wantErr:       sql.ErrNoRows,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()
			sqlxDB := sqlx.NewDb(db, "sqlmock")

			repo := articles.NewRepository(sqlxDB)

			mockExpectQuery := mock.ExpectQuery(tc.expectedQuery)
			if tc.mockGet.err != nil {
				mockExpectQuery.WillReturnError(tc.mockGet.err)
			} else {
				row := sqlmock.NewRows([]string{
					"COUNT(articles.id)",
				})
				result := tc.mockGet.result
				row.AddRow(result)
				mockExpectQuery.WillReturnRows(row)
			}

			got, err := repo.GetArticlesCount(tc.args.ctx, tc.args.search, tc.args.author, tc.args.meta)
			assert.Equal(t, tc.want, got, tc.name)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}

func Test_articleRepository_Create(t *testing.T) {
	type (
		args struct {
			ctx     context.Context
			payload *models.CreateArticlePayload
		}
		mockExec struct {
			err error
		}
	)

	tests := []struct {
		name     string
		args     args
		mockExec mockExec
		wantErr  error
	}{
		{
			name: "success",
			args: args{
				ctx: context.Background(),
				payload: &models.CreateArticlePayload{
					AuthorID: "author-id",
					Title:    "Test",
					Body:     "Test",
				},
			},
			mockExec: mockExec{err: nil},
			wantErr:  nil,
		},
		{
			name: "error",
			args: args{
				ctx: context.Background(),
				payload: &models.CreateArticlePayload{
					AuthorID: "author-id",
					Title:    "Test",
					Body:     "Test",
				},
			},
			mockExec: mockExec{err: sql.ErrConnDone},
			wantErr:  sql.ErrConnDone,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()
			sqlxDB := sqlx.NewDb(db, "sqlmock")

			repo := articles.NewRepository(sqlxDB)

			mockExpectExec := mock.ExpectExec("INSERT INTO articles (id, author_id, title, body, created_at) VALUES (?, ?, ?, ?, NOW())").WithArgs(
				sqlmock.AnyArg(),
				tc.args.payload.AuthorID,
				tc.args.payload.Title,
				tc.args.payload.Body,
			)

			if tc.mockExec.err != nil {
				mockExpectExec.WillReturnError(tc.mockExec.err)
			} else {
				mockExpectExec.WillReturnResult(sqlmock.NewResult(1, 1))
			}

			err = repo.Create(tc.args.ctx, tc.args.payload)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}

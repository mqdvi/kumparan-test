package articles

import (
	"context"

	"gitlab.com/mqdvi/kumparan-test/models"
	"gitlab.com/mqdvi/kumparan-test/pkg/utils"
)

type ArticleRepositoryInterface interface {
	GetArticles(ctx context.Context, search, author *string, meta *utils.Meta) ([]models.Article, error)
	GetArticlesCount(ctx context.Context, search, author *string, meta *utils.Meta) (int64, error)
	Create(ctx context.Context, payload *models.CreateArticlePayload) error
}

type ArticleServiceInterface interface {
	GetArticles(ctx context.Context, query *string, author *string, meta *utils.Meta) (*utils.PaginationResponse, error)
	Create(ctx context.Context, payload *models.CreateArticlePayload) error
}

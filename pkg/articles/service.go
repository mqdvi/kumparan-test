package articles

import (
	"context"
	"database/sql"

	"gitlab.com/mqdvi/kumparan-test/models"
	"gitlab.com/mqdvi/kumparan-test/pkg/authors"
	"gitlab.com/mqdvi/kumparan-test/pkg/utils"
)

type articleService struct {
	repo       ArticleRepositoryInterface
	authorRepo authors.AuthorRepositoryInterface
}

func NewService(repo ArticleRepositoryInterface, authorRepo authors.AuthorRepositoryInterface) *articleService {
	return &articleService{
		repo:       repo,
		authorRepo: authorRepo,
	}
}

func (svc *articleService) GetArticles(ctx context.Context, query *string, author *string, meta *utils.Meta) (*utils.PaginationResponse, error) {
	count, err := svc.repo.GetArticlesCount(ctx, query, author, meta)
	if err != nil {
		return nil, err
	}
	meta.Count = uint64(count)

	articles, err := svc.repo.GetArticles(ctx, query, author, meta)
	if err != nil {
		return nil, err
	}

	var result []models.ArticleResponse
	for _, article := range articles {
		author, err := svc.authorRepo.GetAuthor(ctx, "id", article.AuthorID)
		if err != nil {
			return nil, err
		}

		result = append(result, models.ArticleResponse{
			ID:        article.ID,
			Title:     article.Title,
			Body:      article.Body,
			CreatedAt: article.CreatedAt,
			Author:    author,
		})
	}

	data := utils.Data{}
	data = data.WithMeta(meta)
	data = data.WithItems(result)
	resp := utils.PaginationResponse{
		Data: data,
	}

	return &resp, nil
}

func (svc *articleService) Create(ctx context.Context, payload *models.CreateArticlePayload) error {
	author, err := svc.authorRepo.GetAuthor(ctx, "name", payload.Author)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if author != nil {
		payload.AuthorID = author.ID
	} else {
		newAuthor, err := svc.authorRepo.Create(ctx, payload.Author)
		if err != nil {
			return err
		}
		payload.AuthorID = newAuthor.ID
	}

	err = svc.repo.Create(ctx, payload)
	if err != nil {
		return err
	}

	return nil
}

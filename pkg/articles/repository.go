package articles

import (
	"context"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"gitlab.com/mqdvi/kumparan-test/models"
	"gitlab.com/mqdvi/kumparan-test/pkg/utils"
)

type articleRepository struct {
	db *sqlx.DB
}

func NewRepository(db *sqlx.DB) *articleRepository {
	return &articleRepository{db: db}
}

func (repo *articleRepository) GetArticles(ctx context.Context, search, author *string, meta *utils.Meta) ([]models.Article, error) {
	var (
		query string
		args  []interface{}
	)

	query = `
		SELECT
			articles.id,
			articles.author_id,
			articles.title,
			articles.body,
			articles.created_at
		FROM
			articles
	`
	query, args = repo.appendGetArticlesQuery(query, search, author, meta)

	var articles []models.Article
	if err := repo.db.SelectContext(ctx, &articles, query, args...); err != nil {
		return nil, err
	}

	return articles, nil
}

func (repo *articleRepository) GetArticlesCount(ctx context.Context, search, author *string, meta *utils.Meta) (int64, error) {
	var (
		query string
		args  []interface{}
	)

	query = `
		SELECT
			COUNT(articles.id)
		FROM
			articles
	`
	query, args = repo.appendGetArticlesQuery(query, search, author, meta)

	var result int64
	err := repo.db.Get(&result, query, args...)
	if err != nil {
		return 0, err
	}

	return result, nil
}

func (repo *articleRepository) Create(ctx context.Context, payload *models.CreateArticlePayload) error {
	query := `INSERT INTO articles (id, author_id, title, body, created_at) VALUES (?, ?, ?, ?, NOW())`

	_, err := repo.db.ExecContext(ctx, query,
		uuid.NewString(),
		payload.AuthorID,
		payload.Title,
		payload.Body,
	)
	if err != nil {
		return err
	}

	return nil
}

func (repo *articleRepository) appendGetArticlesQuery(query string, search, author *string, meta *utils.Meta) (string, []interface{}) {
	var args []interface{}

	if search != nil && *search != "" {
		query += `
			WHERE
				(body LIKE ? OR title LIKE ?)
		`
		args = append(args, "%"+*search+"%", "%"+*search+"%")
	}
	if author != nil && *author != "" {
		if search != nil && *search != "" {
			query += `
				AND	
			`
		} else {
			query += `
				WHERE
			`
		}
		query += `
			EXISTS(
				SELECT * FROM authors WHERE articles.author_id = authors.id AND authors.name = ?
			)
		`
		args = append(args, *author)
	}

	query += `
		ORDER BY
			articles.created_at DESC
		LIMIT ?
		OFFSET ?
	`

	args = append(args, meta.Limit, meta.Offset)

	return query, args
}

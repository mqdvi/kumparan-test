package articles_test

import (
	"context"
	"database/sql"
	"errors"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/mqdvi/kumparan-test/mocks"
	"gitlab.com/mqdvi/kumparan-test/models"
	"gitlab.com/mqdvi/kumparan-test/pkg/articles"
	"gitlab.com/mqdvi/kumparan-test/pkg/utils"
)

func Test_articleService_GetArticles(t *testing.T) {
	var (
		meta = utils.Meta{
			Limit:      5,
			Count:      10,
			TotalPages: 2,
			NextLink:   "?itemsPerPage=5&page=1",
			Params:     url.Values{},
			URL:        &url.URL{},
		}
		createdAt = time.Now()
	)

	type (
		args struct {
			ctx    context.Context
			query  *string
			author *string
			meta   *utils.Meta
		}
		mockGetArticlesCount struct {
			result int64
			err    error
		}
		mockGetArticles struct {
			result []models.Article
			err    error
		}
		mockGetAuthor struct {
			result *models.Author
			err    error
		}
	)

	tests := []struct {
		name                 string
		args                 args
		mockGetArticlesCount *mockGetArticlesCount
		mockGetArticles      *mockGetArticles
		mockGetAuthor        *mockGetAuthor
		wantErr              error
	}{
		{
			name: "success",
			args: args{
				ctx: context.Background(),
				meta: &meta,
			},
			mockGetArticlesCount: &mockGetArticlesCount{result: 1},
			mockGetArticles: &mockGetArticles{
				result: []models.Article{{
					ID: "article-id",
					AuthorID: "author-id",
					Title: "Test",
					Body: "Test",
					CreatedAt: createdAt,
				}},
			},
			mockGetAuthor: &mockGetAuthor{result: &models.Author{ID: "author-id"}},
			wantErr: nil,
		},
		{
			name: "error get count",
			args: args{
				ctx: context.Background(),
				meta: &meta,
			},
			mockGetArticlesCount: &mockGetArticlesCount{err: errors.New("error")},
			wantErr: errors.New("error"),
		},
		{
			name: "error get articles",
			args: args{
				ctx: context.Background(),
				meta: &meta,
			},
			mockGetArticlesCount: &mockGetArticlesCount{result: 1},
			mockGetArticles: &mockGetArticles{err: errors.New("error")},
			mockGetAuthor: &mockGetAuthor{result: &models.Author{ID: "author-id"}},
			wantErr: errors.New("error"),
		},
		{
			name: "error get author",
			args: args{
				ctx: context.Background(),
				meta: &meta,
			},
			mockGetArticlesCount: &mockGetArticlesCount{result: 1},
			mockGetArticles: &mockGetArticles{
				result: []models.Article{{
					ID: "article-id",
					AuthorID: "author-id",
					Title: "Test",
					Body: "Test",
					CreatedAt: createdAt,
				}},
			},
			mockGetAuthor: &mockGetAuthor{err: errors.New("error")},
			wantErr: errors.New("error"),
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			articleRepo := new(mocks.ArticleRepositoryInterface)
			authorRepo := new(mocks.AuthorRepositoryInterface)

			if tc.mockGetArticlesCount != nil {
				articleRepo.On("GetArticlesCount", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(tc.mockGetArticlesCount.result, tc.mockGetArticlesCount.err)
			}
			if tc.mockGetArticles != nil {
				articleRepo.On("GetArticles", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(tc.mockGetArticles.result, tc.mockGetArticles.err)
			}
			if tc.mockGetAuthor != nil {
				authorRepo.On("GetAuthor", mock.Anything, mock.Anything, mock.Anything).Return(tc.mockGetAuthor.result, tc.mockGetAuthor.err)
			}

			svc := articles.NewService(articleRepo, authorRepo)

			_, err := svc.GetArticles(tc.args.ctx, tc.args.query, tc.args.author, tc.args.meta)
			assert.Equal(t, err, tc.wantErr)
		})
	}
}

func Test_articleService_Create(t *testing.T) {
	type (
		args struct {
			ctx context.Context
			payload *models.CreateArticlePayload
		}
		mockGetAuthor struct {
			result *models.Author
			err error
		}
		mockCreateAuthor struct {
			result *models.Author
			err error
		}
		mockCreate struct {
			err error
		}
	)

	tests := []struct{
		name string
		args args
		mockGetAuthor *mockGetAuthor
		mockCreateAuthor *mockCreateAuthor
		mockCreate *mockCreate
		wantErr error
	}{
		{
			name: "success",
			args: args{
				ctx: context.Background(),
				payload: &models.CreateArticlePayload{
					Author: "Nayeon",
					Title: "Feel Special",
					Body: "You make me feel special~",
				},
			},
			mockGetAuthor: &mockGetAuthor{
				result: &models.Author{ID: "author-id"},
			},
			mockCreate: &mockCreate{err: nil},
			wantErr: nil,
		},
		{
			name: "success with create new author",
			args: args{
				ctx: context.Background(),
				payload: &models.CreateArticlePayload{
					Author: "Nayeon",
					Title: "Feel Special",
					Body: "You make me feel special~",
				},
			},
			mockGetAuthor: &mockGetAuthor{err: sql.ErrNoRows},
			mockCreateAuthor: &mockCreateAuthor{
				result: &models.Author{ID: "author-id"},
			},
			mockCreate: &mockCreate{err: nil},
			wantErr: nil,
		},
		{
			name: "error create new author",
			args: args{
				ctx: context.Background(),
				payload: &models.CreateArticlePayload{
					Author: "Nayeon",
					Title: "Feel Special",
					Body: "You make me feel special~",
				},
			},
			mockGetAuthor: &mockGetAuthor{err: sql.ErrNoRows},
			mockCreateAuthor: &mockCreateAuthor{err: errors.New("error")},
			wantErr: errors.New("error"),
		},
		{
			name: "error get author",
			args: args{
				ctx: context.Background(),
				payload: &models.CreateArticlePayload{
					Author: "Nayeon",
					Title: "Feel Special",
					Body: "You make me feel special~",
				},
			},
			mockGetAuthor: &mockGetAuthor{err: errors.New("error")},			
			wantErr: errors.New("error"),
		},
		{
			name: "error create article",
			args: args{
				ctx: context.Background(),
				payload: &models.CreateArticlePayload{
					Author: "Nayeon",
					Title: "Feel Special",
					Body: "You make me feel special~",
				},
			},
			mockGetAuthor: &mockGetAuthor{
				result: &models.Author{ID: "author-id"},
			},
			mockCreate: &mockCreate{err: errors.New("error")},
			wantErr: errors.New("error"),
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			articleRepo := new(mocks.ArticleRepositoryInterface)
			authorRepo := new(mocks.AuthorRepositoryInterface)

			if tc.mockGetAuthor != nil {
				authorRepo.On("GetAuthor", mock.Anything, mock.Anything, mock.Anything).Return(tc.mockGetAuthor.result, tc.mockGetAuthor.err)
			}
			if tc.mockCreateAuthor != nil {
				authorRepo.On("Create", mock.Anything, mock.Anything).Return(tc.mockCreateAuthor.result, tc.mockCreateAuthor.err)
			}
			if tc.mockCreate != nil {
				articleRepo.On("Create", mock.Anything, mock.Anything).Return(tc.mockCreate.err)
			}

			svc := articles.NewService(articleRepo, authorRepo)

			err := svc.Create(tc.args.ctx, tc.args.payload)
			assert.Equal(t, err, tc.wantErr)
		})

	}
}

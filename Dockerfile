FROM golang:1.17-alpine as builder

WORKDIR /src

COPY go.mod go.sum /src/
RUN go mod verify \
 && go mod download

COPY . /src/

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
    go build -tags=jsoniter -ldflags "-s -w" -o /app

FROM scratch 

COPY --from=builder /app /app

CMD ["/app"]

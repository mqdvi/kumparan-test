package main

import (
	"context"
	"errors"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"gitlab.com/mqdvi/kumparan-test/pkg/articles"
	"gitlab.com/mqdvi/kumparan-test/pkg/authors"
	"gitlab.com/mqdvi/kumparan-test/pkg/config"
)

func main() {
	// Init database client
	log.Println("Init MySQL connection...")
	db := config.GetDBClient()
	defer db.Close()

	router := gin.New()

	router.Use(gin.Recovery())

	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"*"}
	router.Use(cors.New(config))

	authorRepo := authors.NewRepository(db)
	articleRepo := articles.NewRepository(db)

	articleSvc := articles.NewService(articleRepo, authorRepo)

	articleCtrl := articles.NewController(articleSvc)

	v1 := router.Group("/v1")
	v1.GET("/articles", articleCtrl.GetArticles)
	v1.POST("/articles", articleCtrl.Create)

	srv := &http.Server{
		Addr:         ":4200",
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Printf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal, 1)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")
}
